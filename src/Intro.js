import React, {Component} from 'react';
import {AsyncStorage, StyleSheet} from 'react-native';
import AppIntroSlider from 'react-native-app-intro-slider';
import App from './App';
import * as Utils from './Utils';
import Images from "./Images";

const styles = {
    image: {
        width: 320,
        resizeMode: 'contain'
    }
};

const slides = [
    {
        key: 'game',
        title: 'Instructions',
        text: 'Make sure the UDP network is open on the \nX-Plane 11 Settings screen',
        image: Images.step_game,
        backgroundColor: '#59b2ab',
        imageStyle: {...styles.image, width: '100%', height: 250},
    },
    {
        key: 'security',
        title: 'Firewall Alert!',
        text: 'If the antivirus firewall warns you, allow it.',
        image: Images.security_allow,
        imageStyle: {...styles.image, width: '100%', height: 200},
        backgroundColor: '#febe29',
    },
    {
        key: 'hat',
        title: 'Place the device',
        text: 'Place the device on the hat. And calibrate it. \nThat is all.',
        image: Images.step_place_hat,
        imageStyle: {...styles.image, width: '100%', height: 300},
        backgroundColor: '#22bcb5',
    }
];

export default class extends Component {
    constructor(props) {
        super(props);

        this.state = {
            showIntro: false
        };

        Utils.showIntro = (() => {
            this.setState({showIntro: true})
        }).bind(this);

        AsyncStorage.getItem('introState').then(introState => {
            if (!introState) {
                Utils.showIntro();
            }
        });

    }

    _onDone() {
        // User finished the introduction. Show real app through
        // navigation or simply by controlling state
        this.setState({showIntro: false});
        AsyncStorage.setItem('introState', JSON.stringify(this.state));
    }

    render() {
        if (!this.state.showIntro) {
            return <App/>;
        } else {
            return <AppIntroSlider slides={slides} onDone={this._onDone.bind(this)}/>;
        }
    }
}
