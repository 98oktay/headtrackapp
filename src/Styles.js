
import {StyleSheet} from "react-native";

export default styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        padding: 20,
        paddingVertical: 50
    },
    fullSized: {
        flex: 1,
    },
    formContainer: {
        width: '100%'
    },
    formRow: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'
    },
    titleText: {
        fontFamily: 'Avenir-Black',
        color: '#fff'
    },
    input: {
        borderWidth: 1,
        borderColor: '#ffffff',
        borderRadius: 4,
        backgroundColor: '#ffffff',
        textAlign: 'center',
        padding: 10,
        fontSize: 16,
        marginVertical: 5
    },
    startButton: {
        marginLeft: 'auto',
        marginTop: 5,
        justifyContent:'space-between',
        flexDirection:'row',
        paddingHorizontal: 10,
        alignItems: 'center',
        paddingVertical: 10,
        width: 120,
        borderRadius: 4,
        shadowColor: '#000',
        shadowOffset: {width: 0, height: 2},
        shadowOpacity: 0.2,
        shadowRadius: 4,
        elevation: 1,
        backgroundColor: '#8BC34A'
    },
    button: {
        marginTop: 35,
        marginLeft:'auto',
        marginRight:'auto',
        paddingHorizontal: 0,
        alignItems: 'center',
        paddingVertical: 10,
        width: 120,
        borderRadius: 4,
        shadowColor: '#000',
        shadowOffset: {width: 0, height: 2},
        shadowOpacity: 0.2,
        shadowRadius: 4,
        elevation: 1,
        backgroundColor: '#fff'
    },
    startButtonText: {
        fontFamily: 'Avenir-Black',
        color: '#fff'
    },
    calibrateButton: {
        marginTop: 'auto',
        marginBottom: 'auto',
        position: 'relative',
        zIndex: 10,
        width: 200,
        height: 200,
        alignItems: 'center',
        justifyContent: 'center',
        textAlign: 'center',
        borderRadius: 100,
        shadowColor: '#000',
        shadowOpacity: .2,
        shadowRadius: 0,
        elevation: 1,
        backgroundColor: '#8BC34A',
        borderWidth: 1,
        borderColor: '#fff',
    },
    calibrateText: {
        fontSize: 26,
        textAlign: 'center',
        fontFamily: 'Avenir Next',
        color: '#fff'
    },
    mainRuler: {
        position: 'absolute',
        top: '50%',
        backgroundColor: 'rgba(0,0,0,0.08)',
        width: 1000,
        height: 2,
        alignItems: 'center',
    },
    mainRulerFront: {
        position: 'absolute',
        marginTop: -120
    },
    mainRulerLeft: {
        position: 'absolute',
        right: '50%',
        marginTop: -20,
        marginRight: 120
    },
    mainRulerRight: {
        position: 'absolute',
        left: '50%',
        marginTop: -20,
        marginLeft: 120
    },
    calibrateButtonValues: {
        position:'absolute',
        left:30,
        bottom:40,
        width: '100%',
        textAlign: 'left'
    },
    energySaverText: {
        color: '#fff',
        textAlign:'center'
    },
    energySaverWrapper: {
        flex:1,
        alignItems:'center',
        justifyContent:'center',
        backgroundColor:'#000'
    }

});
