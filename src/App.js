import React, {Component, Fragment} from "react";
import {DeviceAngles} from "NativeModules";
import {
    AppState,
    AsyncStorage,
    DeviceEventEmitter,
    Modal,
    Text,
    TextInput,
    TouchableHighlight,
    TouchableOpacity,
    Platform,
    View
} from "react-native";
import {Buffer} from "buffer";
import LinearGradient from "react-native-linear-gradient";
import Orientation from "react-native-orientation";
import styles from "./Styles";
import dgram from "react-native-udp";
import Slider from "react-native-slider";
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import DeviceBrightness from 'react-native-device-brightness';
import CheckBox from "react-native-check-box";
import * as Utils from './Utils';
import {SensorManager} from 'NativeModules';

console.disableYellowBox = true;


export default class App extends Component {
    constructor(props) {
        super(props);

        this.sleepTimer = null;
        this.anglesData = {
            pitch: 0,
            roll: 0,
            yaw: 0,
            raw_pitch: 0,
            raw_roll: 0,
            raw_yaw: 0,
        };

        this.calibrate = {
            pitch: 0,
            roll: 0,
            yaw: 0,
        };

        this.state = {
            ip: '',
            port: 49000,
            started: false,
            orientation: '',
            yaw_diff: 0,
            pitch: 0,
            roll: 0,
            yaw: 0,
            raw_pitch: 0,
            raw_roll: 0,
            raw_yaw: 0,
            yaw_rate: 1,
            pitch_rate: 1,
            roll_rate: 1,
            settingsVisible: false,
            energySaver: false,
            last_brightnes_state: null,
            appState: '',
            sendOpenTrack: false,
            connection_error: ''
        };

        if (Platform.OS === 'ios') {
            DeviceAngles.setDeviceMotionUpdateInterval(0.05);
            DeviceAngles.startMotionUpdates();
        } else {
            SensorManager.startOrientation(1000 / 60);
        }

        setInterval(() => {

            if (this.state.started) {

                if (this.state.sendOpenTrack) {
                    this._sendOpenTrackData(0, 0, 0, 360 - this.state.yaw, 360 - this.state.roll, 360 - this.state.pitch);
                } else {
                    this._sendData("sim/graphics/view/pilots_head_the", 360 - this.state.roll);
                    this._sendData("sim/graphics/view/pilots_head_psi", 360 - this.state.yaw);
                    this._sendData("sim/graphics/view/pilots_head_phi", 360 - this.state.pitch);
                }

            }


        }, 1000 / 60);

        this._wakeScreen = this._wakeScreen.bind(this);
        this._waitForSleep = this._waitForSleep.bind(this);
        this._handleAppStateChange = this._handleAppStateChange.bind(this);
        this._settingsUpdate = this._settingsUpdate.bind(this);

    }

    num_0_360(num) {
        return (num + 360) % 360;
    }


    num_multipler(num, rate) {

        const d = num > 180 ? -(360 - num) : num;

        return this.num_0_360(d * rate);
    }

    componentDidMount() {

        // Android events
        DeviceEventEmitter.addListener('Orientation', (data) => {

            const pitch = this.num_0_360(data.pitch);
            const roll = this.num_0_360(data.roll);
            const yaw = this.num_0_360(data.azimuth);

            this.anglesData = {
                raw_pitch: pitch,
                raw_roll: roll,
                raw_yaw: yaw,
                pitch: this.num_multipler(this.num_0_360(pitch - this.calibrate.pitch), this.state.pitch_rate),
                roll: this.num_multipler(this.num_0_360(roll - this.calibrate.roll), this.state.roll_rate),
                yaw: this.num_multipler(this.num_0_360(yaw - this.calibrate.yaw), this.state.yaw_rate),
            };
        });

        DeviceEventEmitter.addListener('AnglesData', (data) => {

            const pitch = this.num_0_360(data.pitch);
            const roll = this.num_0_360(data.roll);
            const yaw = this.num_0_360(data.yaw);

            this.anglesData = {
                raw_pitch: pitch,
                raw_roll: roll,
                raw_yaw: yaw,
                pitch: this.num_multipler(this.num_0_360(pitch - this.calibrate.pitch), this.state.pitch_rate),
                roll: this.num_multipler(this.num_0_360(roll - this.calibrate.roll), this.state.roll_rate),
                yaw: this.num_multipler(this.num_0_360(yaw - this.calibrate.yaw), this.state.yaw_rate),
            };
        });

        const orientation = Orientation.getInitialOrientation();
        this.setState({orientation});
        Orientation.addSpecificOrientationListener(this._orientationDidChange.bind(this));


        AsyncStorage.getItem('ipdata').then(dataStr => {

            const data = JSON.parse(dataStr);

            if (data) {

                this.setState({
                    ip: data.ip,
                    port: parseInt(data.port, 10),
                    sendOpenTrack: data.sendOpenTrack
                })
            }

        });

        setInterval(() => {

            this.setState({
                ...this.anglesData
            });

        }, 1000 / 30);

        AppState.addEventListener('change', this._handleAppStateChange);

    }

    _orientationDidChange(sb_orientation) {
        this.setState({
            orientation: sb_orientation,
            //yaw_diff: sb_orientation === 'PORTRAIT' ? 0 : sb_orientation === 'LANDSCAPE-LEFT' ? 90 : -90
        });
    }

    componentWillUnmount() {
        Orientation.removeSpecificOrientationListener(this._orientationDidChange);
        AppState.removeEventListener('change', this._handleAppStateChange);
        if (this.client) {
            this.client.close();
            this.setState({
                started: false,
                connection_error: 'Connection closed.'
            })
        }


    }

    _handleAppStateChange(nextAppState) {

        if (this.state.appState.match(/inactive|background/) && nextAppState === 'active') {
            this._wakeScreen();
        } else {
            if (this.state.last_brightnes_state) {
                DeviceBrightness.setBrightnessLevel(this.state.last_brightnes_state);
            }
            this._wakeScreen();
        }
    }

    _startStop() {

        if (this.client) {
            this.client.close();
        }
        if (!this.state.started) {


            this.client = dgram.createSocket('udp4');

            AsyncStorage.setItem("ipdata", JSON.stringify({
                ip: this.state.ip,
                port: this.state.port,
                sendOpenTrack: this.state.sendOpenTrack
            }), (err) => {
            });

        } else {

            Orientation.unlockAllOrientations();

        }

        this.setState({
            started: !this.state.started,
            connection_error: ''
        }, this._waitForSleep);

    }

    _calibrate() {
        this.calibrate = {
            pitch: this.state.raw_pitch,
            roll: this.state.raw_roll,
            yaw: this.state.raw_yaw,
        };

        this._waitForSleep();
    }

    _modalClose() {
        this.setState({
            settingsVisible: false
        });
        this._waitForSleep();
    }

    _wakeScreen() {
        this.setState({
            energySaver: false
        });

        if (this.state.last_brightnes_state) {
            DeviceBrightness.setBrightnessLevel(this.state.last_brightnes_state);
        }

        this._waitForSleep();
    }

    _waitForSleep() {

        if (this.sleepTimer) {
            clearTimeout(this.sleepTimer);
        }
        if (this.state.started) {

            this.sleepTimer = setTimeout(() => {
                DeviceBrightness.getBrightnessLevel().then(brightness => {
                    DeviceBrightness.setBrightnessLevel(0.1);
                    this.setState({
                        last_brightnes_state: brightness,
                        energySaver: true,
                        settingsVisible: false,
                    });
                })

            }, 10000);

        }
    }

    _modalShow() {
        this.setState({
            settingsVisible: true
        });
        this._waitForSleep();
    }

    _settingsUpdate (values){
        this.setState(values);
        this._waitForSleep();
    }

    _sendData(key, value) {
        const buf = Buffer.alloc(509);
        buf.fill(0);
        buf.writeFloatLE(value, 5);
        buf.write("DREF", 0);
        buf.write(key, 9);
        try {
            this.client.send(buf, 0, buf.length, this.state.port, this.state.ip);
        } catch (e) {
            if (this.state.started) {
                this._startStop();
                this.setState({
                    connection_error: e.message
                });
            }

        }

    }

    _sendOpenTrackData(x, y, z, yaw, pitch, roll) {
        const buf = Buffer.alloc(56);
        buf.fill(0);
        buf.writeDoubleLE(x, 0);
        buf.writeDoubleLE(y, 8);
        buf.writeDoubleLE(z, 16);
        buf.writeDoubleLE(yaw, 24);
        buf.writeDoubleLE(pitch, 32);
        buf.writeDoubleLE(roll, 40);
        try {
            this.client.send(buf, 0, buf.length, this.state.port, this.state.ip);
        } catch (e) {
            if (this.state.started) {
                this._startStop();
                this.setState({
                    connection_error: e.message
                });
            }
        }
    }

    render() {
        const isValidInput = (this.state.ip && this.state.port && !isNaN(this.state.port) && this.state.port > 0 && this.state.ip.length > 1);
        return this.state.energySaver ?
            <TouchableOpacity activeOpacity="1" onPress={this._wakeScreen} style={styles.energySaverWrapper}><Text
                style={styles.energySaverText}>Connected...{'\n\n'}Screen dimmed to save energy.{'\n\n'}Tap to
                wake.</Text></TouchableOpacity> : (
                <LinearGradient colors={['#62a0b5', '#62a0b5', '#ffffff',]}
                                style={styles.container}>
                    {this.state.orientation === 'PORTRAIT' && <View style={styles.formContainer}>
                        <View style={styles.formRow}><Text style={styles.titleText}>Game Hostname / Port</Text></View>
                        <View style={styles.formRow}>
                            <TextInput editable={!this.state.started}
                                       placeholder="Example: 10.0.0.35"
                                       textContentType="url"
                                       style={[styles.input, {flex: 1, opacity: this.state.started ? 0.5 : 1}]}
                                       value={this.state.ip}
                                       onChangeText={(ip) => {
                                           this.setState({ip: ip.replace(/([^a-zA-Z0-9\.\:\%\[\]]+)/mig, '')});
                                       }}
                            />
                            <TextInput keyboardType="number-pad" editable={!this.state.started}
                                       placeholder="Ex:49000"
                                       style={[styles.input, {
                                           width: 120,
                                           marginLeft: 10,
                                           opacity: this.state.started ? 0.5 : 1
                                       }]}
                                       onChangeText={(port) => this.setState({port: parseInt(port.replace(/([^0-9]+)/mgi, ''), 10)})}
                                       value={!isNaN(this.state.port) ? this.state.port.toString() : ''}/>
                        </View>
                        <View style={styles.formRow}>
                            {this.state.started ? <Text style={styles.titleText}>Sending Data...</Text> :
                                this.state.connection_error ?
                                    <Text style={[styles.titleText, {
                                        color: '#ff0000'
                                    }]}>{this.state.connection_error}</Text> :
                                    <TouchableOpacity style={styles.formRow} onPress={() => {
                                        this.setState({sendOpenTrack: !this.state.sendOpenTrack});
                                    }}><CheckBox
                                        isChecked={this.state.sendOpenTrack}
                                        onClick={() => {
                                        }}
                                        checkBoxColor="#ffffff"
                                    /><Text style={styles.titleText}>{' '}OpenTrack UDP</Text></TouchableOpacity>}


                            <TouchableOpacity disabled={!isValidInput}
                                              style={[styles.startButton, {opacity: isValidInput ? 1 : 0.3}]}
                                              onPress={this._startStop.bind(this)}>
                                {this.state.started ? <Fragment>
                                    <Text style={styles.startButtonText}>STOP</Text>
                                    <Icon name="stop" size={20} color="#FFFFFF"/>
                                </Fragment> : <Fragment>
                                    <Text style={styles.startButtonText}>CONNECT</Text>
                                    <Icon name="upload" size={20} color="#FFFFFF"/>
                                </Fragment>}
                            </TouchableOpacity>
                        </View>
                    </View>
                    }
                    <TouchableOpacity onPress={this._calibrate.bind(this)} style={[styles.calibrateButton, {
                        shadowOffset: this.state.orientation === 'PORTRAIT' ? {
                            width: -(this.state.roll > 180 ? -(360 - this.state.roll) : this.state.roll),
                            height: -(this.state.pitch > 180 ? -(360 - this.state.pitch) : this.state.pitch),
                        } : {
                            height: (this.state.roll > 180 ? -(360 - this.state.roll) : this.state.roll),
                            width: (this.state.pitch > 180 ? -(360 - this.state.pitch) : this.state.pitch),
                        }
                    }]}>
                        <Text style={styles.calibrateText}>Calibrate</Text>
                        <View
                            style={[styles.mainRuler, {transform: [{rotate: `${Math.round((this.state.yaw + this.state.yaw_diff) * 100) / 100}deg`}]}]}>
                            <Text style={styles.mainRulerFront}>FRONT</Text>
                            <Text style={styles.mainRulerLeft}>LEFT</Text>
                            <Text style={styles.mainRulerRight}>RIGHT</Text>
                        </View>

                    </TouchableOpacity>


                    <View style={styles.calibrateButtonValues}>
                        <Text>PITCH: {Math.round(this.state.pitch)}</Text>
                        <Text>ROLL: {Math.round(this.state.roll)}</Text>
                        <Text>YAW: {Math.round(this.state.yaw)}</Text>
                    </View>

                    {this.state.orientation === 'PORTRAIT' && <TouchableOpacity onPress={this._modalShow.bind(this)}>
                        <Text>
                            <Icon name="cog" size={30} color="#8BC34A"/>
                        </Text>
                    </TouchableOpacity>}

                    <Modal animationType="slide"
                           transparent={true}
                           onRequestClose={this._modalClose.bind(this)}
                           visible={this.state.settingsVisible}>

                        <View style={{
                            backgroundColor: '#fff',
                            borderRadius: 10,
                            margin: 'auto',
                            marginTop: 'auto',
                            padding: 20,
                            paddingBottom: 50,
                        }}>

                            <Text style={[styles.titleText, {color: '#666'}]}>Sensitivity</Text>
                            <View style={styles.formRow}>
                                <Text>YAW </Text>
                                <Slider
                                    style={{flex: 1}}
                                    step={0.1}
                                    value={this.state.yaw_rate}
                                    minimumValue={0.1}
                                    maximumValue={3.0}
                                    thumbTintColor="#8BC34A"
                                    onValueChange={yaw_rate => this._settingsUpdate({yaw_rate})}
                                />
                                <Text>{this.state.yaw_rate.toFixed(2)}</Text>
                            </View>
                            <View style={styles.formRow}>
                                <Text>ROLL </Text>
                                <Slider
                                    style={{flex: 1}}
                                    step={0.1}
                                    value={this.state.roll_rate}
                                    minimumValue={0.1}
                                    maximumValue={3.0}
                                    thumbTintColor="#8BC34A"
                                    onValueChange={roll_rate => this._settingsUpdate({roll_rate})}
                                />
                                <Text>{this.state.roll_rate.toFixed(2)}</Text>

                            </View>
                            <View style={styles.formRow}>
                                <Text>PITCH </Text>
                                <Slider
                                    style={{flex: 1}}
                                    step={0.1}
                                    value={this.state.pitch_rate}
                                    minimumValue={0.1}
                                    maximumValue={3.0}
                                    thumbTintColor="#8BC34A"
                                    onValueChange={pitch_rate => this._settingsUpdate({pitch_rate})}
                                />
                                <Text>{this.state.pitch_rate.toFixed(2)}</Text>

                            </View>

                            <TouchableOpacity onPress={this._modalClose.bind(this)}
                                              style={{marginLeft: 'auto', marginRight: 'auto'}}>
                                <Text>
                                    <Icon name="times" size={30} color="#8B0000"/>
                                </Text>
                            </TouchableOpacity>

                        </View>

                    </Modal>
                    <TouchableOpacity onPress={Utils.showIntro} style={{
                        flexDirection: 'row',
                        alignItems: 'center',
                        position: 'absolute',
                        right: 30,
                        bottom: 50
                    }}><Icon name="question" size={20} color="#333"/><Text
                        style={[styles.titleText, {color: '#333'}]}>{' '}Help</Text></TouchableOpacity>
                </LinearGradient>
            );
    }
}