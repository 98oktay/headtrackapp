/** @format */

import {AppRegistry} from 'react-native';
import Intro from './src/Intro';
import {name as appName} from './app.json';

AppRegistry.registerComponent(appName, () => Intro);
